package com.example.julie.busbookingapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class SlideAdapter extends PagerAdapter {

    Context context;
    LayoutInflater layoutInflater;

    public SlideAdapter(Context context) {

        this.context = context;

    }

    // Arrays
    public int[] slide_images = {

            R.drawable.welcome_icon,
            R.drawable.destination_icon,
            R.drawable.setup_icon,
            R.drawable.payment_icon,
            R.drawable.verification_icon
    };

    public String[] slide_headings = {

            "WELCOME",
            "SEARCH FOR A DESTINATION",
            "CHOOSE FOR DESTINATION SETUP",
            "CHOOSE SEATS & PAYMENT METHOD",
            "VERIFICATION"

    };

    public String[] slide_descs = {

            "BUS TO GO APPLICATION is a great application for you travelers to book seats for your desired travels. " +
                    "This starting guides will show/teach you how to use BUS TO GO.",

            "You can simply \"Search\" your preferred destination through the input fields shown at the start of the " +
                    "BUS TO GO application.",

            "After searching your desired destination, you can choose whether you want to travel on an airconditioned bus " +
                    "or ordinary, daytime or night-time, and also pick your fare price from different bus companies/operators.",

            "After choosing your destination setup, you can arrange yourself/selves what seats you prefer. " +
                    "And also select the payment method you wanted to use for the payment transaction.",

            "After choosing your desired seats and payment method, we will verify you if you are a legitimate passenger. " +
                    "By simply putting up your email address on us and verified your email."

    };



    @Override
    public int getCount() {
        return slide_headings.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == (RelativeLayout) o;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.slide_layout, container, false);

        ImageView slideImageView = (ImageView) view.findViewById(R.id.slide_image);
        TextView slideHeading = (TextView) view.findViewById(R.id.slide_heading);
        TextView slideDescription = (TextView) view.findViewById(R.id.slide_desc);

        slideImageView.setImageResource(slide_images[position]);
        slideHeading.setText(slide_headings[position]);
        slideDescription.setText(slide_descs[position]);

        container.addView(view);


        return view;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {

        container.removeView((RelativeLayout)object);
    }
}
