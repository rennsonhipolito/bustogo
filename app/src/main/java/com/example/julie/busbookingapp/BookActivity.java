package com.example.julie.busbookingapp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class BookActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private Button searchbtn;
    private EditText destinationtext;
    private EditText origintext;
    private Button selectbrn;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);


        searchbtn = (Button) findViewById(R.id.search_btn);
        destinationtext = (EditText) findViewById(R.id.destination_text);
        origintext = (EditText) findViewById(R.id.origin_text);



        recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        recyclerView.setLayoutManager(layoutManager);

           List<ModelClass> modelClassList = new ArrayList<>();
        modelClassList.add(new ModelClass("Baguio", "Cubao", "2019-01-01", "00:00", "10001"));
        modelClassList.add(new ModelClass("Quiapo", "Cubao", "2019-01-02", "01:01", "1000"));
        modelClassList.add(new ModelClass("Taguig", "Baguio", "2019-01-03", "02:02", "100"));
        modelClassList.add(new ModelClass("Baguio", "Pampanga", "2019-01-04", "03:03", "10"));
        modelClassList.add(new ModelClass("Batangas", "Cubao", "2019-01-05", "04:04", "1001"));
        modelClassList.add(new ModelClass("Baguio", "Batangas", "2019-02-05", "05:05", "101"));
        modelClassList.add(new ModelClass("Bulacan", "Cubao", "2019-03-03", "06:06", "11"));
        modelClassList.add(new ModelClass("Baguio", "Bulacan", "2019-04-01", "07:07", "10012"));
        modelClassList.add(new ModelClass("Taguig", "Cubao", "2019-01-12", "12:12", "1"));



        Adapter adapter = new Adapter(modelClassList, this);
        recyclerView.setAdapter(adapter);





    }


}
