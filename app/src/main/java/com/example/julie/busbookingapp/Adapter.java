package com.example.julie.busbookingapp;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.Viewholder> {

    private List<ModelClass> modelClassList;
    Context context;

    public Adapter(List<ModelClass> modelClassList, Context context) {

        this.modelClassList = modelClassList;
        this.context = context;
    }


    @Override
    public Viewholder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_layout, parent, false);
        Viewholder viewholder = new Viewholder(view, context, (ArrayList<ModelClass>) modelClassList);

        return viewholder;


    }

    @Override
    public void onBindViewHolder(Viewholder holder, int position) {

        String description = modelClassList.get(position).getDestination();
        String origin = modelClassList.get(position).getOrigin();
        String date = modelClassList.get(position).getDeparturedate();
        String time = modelClassList.get(position).getDeparturetime();
        String price = modelClassList.get(position).getPrice();

        holder.setData(description, origin, date, time, price);
    }

    @Override
    public int getItemCount() {
        return modelClassList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private TextView destination;
        private TextView origin;
        private TextView date;
        private TextView time;
        private TextView price;
        Context context;
        ArrayList<ModelClass> list;

        public Viewholder(View itemView, Context context, ArrayList<ModelClass> list) {
            super(itemView);

            destination = itemView.findViewById(R.id.destination_text);
            origin = itemView.findViewById(R.id.origin_text);
            date = itemView.findViewById(R.id.date_text);
            time = itemView.findViewById(R.id.time_text);
            price = itemView.findViewById(R.id.price_text);
            itemView.setOnClickListener(this);
            this.context = context;
            this.list = list;

        }

        private void setData(String destinationText, String originText, String dateText, String timeText, String priceText){

            destination.setText(destinationText);
            origin.setText(originText);
            date.setText(dateText);
            time.setText(timeText);
            price.setText(priceText);
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, BookDetails.class);
            intent.putExtra("destination", list.get(getAdapterPosition()).getDestination());
            intent.putExtra("origin", list.get(getAdapterPosition()).getOrigin());
            intent.putExtra("date", list.get(getAdapterPosition()).getDeparturedate());
            intent.putExtra("time", list.get(getAdapterPosition()).getDeparturetime());
            intent.putExtra("price", list.get(getAdapterPosition()).getPrice());
            context.startActivity(intent);
        }
    }

}
