package com.example.julie.busbookingapp;

public class ModelClass{

        private String Destination;
        private String Origin;
        private String Departuredate;
        private String Departuretime;
        private String Price;


        public ModelClass(String destination, String origin, String departuredate, String departuretime, String price) {
            this.Destination = destination;
            this.Origin = origin;
            this.Departuredate = departuredate;
            this.Departuretime = departuretime;
            this.Price = price;
        }


        public String getDestination() {
            return Destination;
        }

        public String getOrigin() {
            return Origin;
        }

        public String getDeparturedate() {
            return Departuredate;
        }

        public String getDeparturetime() {
            return Departuretime;
        }

        public String getPrice() {
            return Price;
        }
}
