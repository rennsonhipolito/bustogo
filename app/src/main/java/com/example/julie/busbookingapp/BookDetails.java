package com.example.julie.busbookingapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

public class BookDetails extends AppCompatActivity {

    private TextView destinationDetails;
    private TextView originDetails;
    private TextView dateDetails;
    private TextView timeDetails;
    private TextView priceDetails;
    private EditText nameEt;
    private EditText emialEt;
    private EditText noseatEt;
    private EditText seatnumEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_details);


        destinationDetails.setText(getIntent().getStringExtra("destination"));
        originDetails.setText(getIntent().getStringExtra("origin"));
        dateDetails.setText(getIntent().getStringExtra("date"));
        timeDetails.setText(getIntent().getStringExtra("time"));
        priceDetails.setText(getIntent().getStringExtra("price"));
    }
}
